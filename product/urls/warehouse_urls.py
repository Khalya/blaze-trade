from django.urls import path

from product.views import WarehouseListView, WarehouseDetailView

urlpatterns = [
    path('warehouse/', WarehouseListView.as_view(), name='warehouse'),
    path('warehouse/<int:pk>/', WarehouseDetailView.as_view(), name='warehouse_products'),
]