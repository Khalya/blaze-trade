from django.forms import ModelForm

from product.models import Warehouse, Product, Delivery, ProductReturn, ProductCountMessage


class WarehouseModelForm(ModelForm):
    class Meta:
        model = Warehouse
        fields = '__all__'


class ProductModelForm(ModelForm):
    class Meta:
        model = Product
        exclude = 'difference', 'purchased', 'remainder'


class DeliveryModelForm(ModelForm):
    class Meta:
        model = Delivery
        exclude = 'product',


class ProductReturnForm(ModelForm):
    class Meta:
        model = ProductReturn
        exclude = 'product',

class ProductCountMessageModelForm(ModelForm):
    class Meta:
        model = ProductCountMessage
        fields = '__all__'