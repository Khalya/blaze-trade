from django.urls import path, include

urlpatterns = [
    path('', include('product.urls.products_urls')),
    path('', include('product.urls.warehouse_urls'))
]
