from django.contrib import admin

from product.models import Product, Delivery, Sale, ProductCount


@admin.register(Product)
class ProductModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'article', 'wholesale_price', 'retail_price']
    list_search = ['title', 'article']
    readonly_fields = ['difference', 'remainder', 'purchased']


@admin.register(Delivery)
class DeliveryModelAdmin(admin.ModelAdmin):
    pass


@admin.register(Sale)
class SaleModelAdmin(admin.ModelAdmin):
    pass

@admin.register(ProductCount)
class ProductCountModelAdmin(admin.ModelAdmin):
    pass
