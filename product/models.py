from datetime import datetime
from django.db import models

from core.models import Balance, Cash
from core.utils import SingletonModel
from user.models import User


class Warehouse(models.Model):
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )
    address = models.CharField(
        verbose_name='Адресс (необязательно)',
        max_length=255,
        null=True,
        blank=True
    )
    phone = models.CharField(
        verbose_name='Телефон (необязательно)',
        max_length=255,
        null=True,
        blank=True
    )
    comment = models.TextField(
        verbose_name='Комментарий (необязательно)',
        null=True,
        blank=True
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Склад'
        verbose_name_plural = 'Склады'


class Product(models.Model):
    class Unit(models.TextChoices):
        unit = 'unit', 'шт.'
        killogram = 'kg', 'кг.'
        liter = 'lt', 'л.'

    article = models.CharField(
        verbose_name='Артикуль',
        max_length=255,
        unique=True
    )
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )
    unit = models.CharField(
        verbose_name='Единица измерения',
        choices=Unit.choices,
        default='unit',
        max_length=255
    )
    retail_price = models.FloatField(
        verbose_name='Цена в розницу',
        default=0
    )
    wholesale_price = models.FloatField(
        verbose_name='Цена оптом',
        default=0
    )
    difference = models.FloatField(
        verbose_name='Разница',
        default=0
    )
    purchased = models.PositiveIntegerField(
        verbose_name='Куплено',
        default=0
    )
    remainder = models.IntegerField(
        verbose_name='Общее количество',
        default=0
    )

    def __str__(self):
        return f"{self.title} - {self.article}"

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.remainder = sum([x.count for x in self.deliveries.all()]) - self.purchased - sum(
            [x.return_count for x in self.returns.all()])
        self.difference = self.retail_price - self.wholesale_price
        product_count = ProductCountMessage.objects.first()
        super().save(force_insert=False, force_update=False, using=None,
                     update_fields=None)
        if self.messages.all() and self.remainder >= (product_count.count if product_count else 45):
            self.messages.all().delete()
        if self.remainder <= (product_count.count if product_count else 45):
            Message.objects.get_or_create(product_id=self.id, defaults={
                'product_id': self.id,
                'title': "Нехватка продуктов !",
                "content": f"На складе осталось всего {self.remainder} {self.get_unit_display()} {self.title} \n нужно пополнить запасы !"
            })

    class Meta:
        verbose_name = 'Продукт'
        verbose_name_plural = 'Продукты'


class Delivery(models.Model):
    product = models.ForeignKey(
        Product,
        verbose_name='Продукт',
        on_delete=models.CASCADE,
        related_name='deliveries'
    )
    count = models.PositiveIntegerField(
        verbose_name='Количество',
        default=0
    )
    created = models.DateTimeField(
        verbose_name='Время завоза',
        default=datetime.now,
        null=True,
        blank=True
    )
    warehouse = models.ForeignKey(
        to=Warehouse,
        verbose_name='Склад',
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return f"{self.product.title} | {self.count} | {self.created}"

    class Meta:
        verbose_name = 'Завоз товара'
        verbose_name_plural = 'Завозы товаров'


class ProductCount(models.Model):
    product = models.ForeignKey(
        Product,
        verbose_name='Продукт',
        on_delete=models.CASCADE,
        related_name='product_count'
    )
    kg = models.FloatField(
        verbose_name='Масса',
        default=0
    )
    count = models.IntegerField(
        verbose_name='Количество ',
        default=0
    )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.product.purchased += self.count
        self.product.remainder -= self.count
        self.product.save()
        super().save(force_insert=False, force_update=False, using=None,
                     update_fields=None)

    def __str__(self):
        return str(self.count)

    class Meta:
        verbose_name = 'Количество Продукт'
        verbose_name_plural = 'Количество Продукты'


class Sale(models.Model):
    class PaymentTypeChoices(models.TextChoices):
        classic = 'classic', 'Наличкой'
        card = 'card', 'Картой'

    products = models.ManyToManyField(
        to=ProductCount,
        verbose_name='Продукты',
        related_name='sales'
    )
    payment_type = models.CharField(
        verbose_name='Тип оплаты',
        max_length=10,
        choices=PaymentTypeChoices.choices
    )
    amount = models.FloatField(
        verbose_name='Общая сумма',
        default=0
    )
    cash = models.ForeignKey(
        'core.Cash',
        verbose_name='Касса',
        on_delete=models.SET_NULL,
        null=True
    )
    created = models.DateField(
        verbose_name='Создано',
        auto_now_add=True,
        null=True,
        blank=True
    )

    def __str__(self):
        return f"{str(self.created)}"

    class Meta:
        verbose_name = 'Продажа'
        verbose_name_plural = 'Продажи'


class ProductReturn(models.Model):
    product = models.ForeignKey(
        Product,
        verbose_name='Продукт',
        on_delete=models.CASCADE,
        related_name='returns'
    )
    return_count = models.PositiveIntegerField(
        verbose_name='Количество',
        default=0
    )
    created = models.DateTimeField(
        verbose_name='Время возврата'
    )
    warehouse = models.ForeignKey(
        to=Warehouse,
        verbose_name='Склад',
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return str(self.return_count)

    class Meta:
        verbose_name = 'Возврат товара'
        verbose_name_plural = 'Возврат товара'


### Доп. настройки пользователя
class ProductCountMessage(SingletonModel):
    count = models.PositiveIntegerField(
        verbose_name='При каком количестве товара уведомлять вас о его нехватке',
        default=45
    )

    def __str__(self):
        return str(self.count)

    class Meta:
        verbose_name = 'Уведомление при каком количестве товаров'
        verbose_name_plural = 'Уведомление при каком количетве товаров'


class Theme(models.Model):
    class ThemeChoices(models.TextChoices):
        dark = 'dark', 'Темная тема'
        white = 'light', 'Светлая тема'

    user = models.ForeignKey(
        User,
        verbose_name='пользователь',
        on_delete=models.CASCADE
    )
    theme = models.CharField(
        verbose_name='Тема системы',
        choices=ThemeChoices.choices,
        default='light',
        max_length=255
    )


class Message(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=255)
    product = models.ForeignKey(
        to=Product,
        verbose_name='Продукт',
        related_name='messages',
        on_delete=models.CASCADE
    )
    content = models.TextField(
        verbose_name='Содержание',
    )

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = 'Уведомление'
        verbose_name_plural = 'Уведомления'


class StartSum(models.Model):
    cash = models.ForeignKey(
        Cash,
        verbose_name='Касса',
        on_delete=models.CASCADE
    )
    amount = models.FloatField(
        verbose_name='Начальная сумма'
    )

    def __str__(self):
        return self.amount

    class Meta:
        verbose_name = 'Начальная сумма в кассе'
        verbose_name_plural = 'Начальная сумма в кассе'


class CustomManager(models.Manager):
    def get_or_create(self, defaults=None, **kwargs):
        obj, created = super().get_or_create(defaults, **kwargs)
        total = 0
        products = Product.objects.all()
        for product in products:
            total += product.retail_price * product.remainder
        obj.amount = total
        obj.save()
        return obj, created


class TurnOver(models.Model):
    amount = models.FloatField(
        verbose_name='Сумма',
        default=0
    )
    objects = CustomManager()

    def __str__(self):
        return str(self.amount)

    class Meta:
        verbose_name = 'Общий оборот'
        verbose_name_plural = 'Общий оборот'
