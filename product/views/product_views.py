import datetime

import qsstats
from dateutil.relativedelta import relativedelta
from django.db.models import Sum
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import DetailView, TemplateView

from core.utils.helpers import get_statistics_data_product
from product.forms import ProductModelForm, DeliveryModelForm, ProductReturnForm
from product.models import Product, Delivery, ProductReturn, Sale, Warehouse


class ProductListView(TemplateView):
    template_name = 'products/product.html'

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        context['form'] = ProductModelForm()
        context['products'] = Product.objects.order_by('-id')
        context['warehouses'] = Warehouse.objects.order_by('-id')
        return context


def create_product(request):
    form = ProductModelForm(request.POST)
    delivery_counts = request.POST.get('delivery_count', None)
    if form.is_valid():
        instance = form.save()
        Delivery.objects.create(
            product=instance,
            count=delivery_counts,
            warehouse_id=request.POST.get('warehouse'),
        )
        instance.save()
        return redirect(reverse_lazy('products'))
    else:
        print(form.errors)


class ProductDetailView(DetailView):
    model = Product
    fields = '__all__'
    template_name = 'products/product-detail.html'
    context_object_name = 'product'

    def get_context_data(self, **kwargs):
        context = super(ProductDetailView, self).get_context_data(**kwargs)
        context['form'] = ProductModelForm(instance=self.object)
        context['delivery_form'] = DeliveryModelForm()
        context['return_product_form'] = ProductReturnForm()
        warehouse = self.request.GET.get('delivery_warehouse', None)
        context['warehouses'] = Warehouse.objects.exclude(id=warehouse)
        context['warehouse'] = Warehouse.objects.get(id=warehouse)
        context['deliveries'] = Delivery.objects.filter(warehouse_id=warehouse, product_id=self.kwargs['pk'])
        context['returns'] = ProductReturn.objects.filter(warehouse_id=warehouse, product_id=self.kwargs['pk'])
        self.object.save()
        return context

    def post(self, request, pk):
        warehouse = Warehouse.objects.first()
        response = redirect(reverse_lazy('product_detail', kwargs={'pk': pk}) + f"?delivery_warehouse={warehouse.id}")
        object = Product.objects.get(pk=pk)
        if request.POST.get('delete_product', None):
            object = Product.objects.get(pk=pk)
            object.delete()
            return redirect(reverse_lazy('products'))
        elif request.POST.get('delete', None) and request.POST.get('type', None):
            delivery = request.POST.get('return', None)
            ProductReturn.objects.get(id=delivery).delete()
            return response
        elif request.POST.get('delete', None):
            delivery = request.POST.get('delivery', None)
            Delivery.objects.get(id=delivery).delete()
            return response
        form = ProductModelForm(request.POST, instance=object)
        delivery_form = DeliveryModelForm(request.POST)
        return_product_form = ProductReturnForm(request.POST)
        if form.is_valid():
            form.save()
            return response
        elif delivery_form.is_valid():
            obj = delivery_form.save(commit=False)
            obj.product_id = pk
            obj.save()
            return response
        elif return_product_form.is_valid():
            obj = return_product_form.save(commit=False)
            obj.product_id = pk
            obj.save()
            return response


class ProductAnalyticView(TemplateView):
    template_name = 'analytic/analytic.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product_get = self.request.GET.get('product', None)
        if product_get:

            #### Simple Data
            product = Product.objects.get(id=product_get)
            context['product'] = product
            context['product_id'] = product_get
            context['returns_count'] = \
            ProductReturn.objects.filter(id=product_get).aggregate(returns_count=Sum('return_count'))['returns_count']
            context['products'] = Product.objects.exclude(id=product_get)

            whosale_amount, retail_amount, sales_count = get_statistics_data_product(product)
            context['whosale_amount_day'] = whosale_amount
            context['retail_amount_day'] = retail_amount
            context['sales_count_day'] = sales_count

            whosale_amount_yesterday, retail_amount_yesterday, sales_count_yesterday \
                = get_statistics_data_product(product, interval='yesterday')
            context['whosale_amount_yesterday'] = whosale_amount_yesterday
            context['retail_amount_yesterday'] = retail_amount_yesterday
            context['sales_count_yesterday'] = sales_count_yesterday

            whosale_amount_weeks, retail_amount_weeks, sales_count_weeks \
                = get_statistics_data_product(product, interval='weeks')
            context['whosale_amount_weeks'] = whosale_amount_weeks
            context['retail_amount_weeks'] = retail_amount_weeks
            context['sales_count_weeks'] = sales_count_weeks

            whosale_amount_month, retail_amount_month, sales_count_month \
                = get_statistics_data_product(product, interval='month')
            context['whosale_amount_month'] = whosale_amount_month
            context['retail_amount_month'] = retail_amount_month
            context['sales_count_month'] = sales_count_month

            whosale_amount_years, retail_amount_years, sales_count_years \
                = get_statistics_data_product(product, interval='years')
            context['whosale_amount_years'] = whosale_amount_years
            context['retail_amount_years'] = retail_amount_years
            context['sales_count_years'] = sales_count_years

            start_date = self.request.GET.get('start_date', None)
            end_date = self.request.GET.get('end_date', None)
            context['start_date'] = start_date
            context['end_date'] = end_date
            if start_date and end_date:
                whosale_amount_dates, retail_amount_dates, sales_count_dates \
                    = get_statistics_data_product(product, date=[start_date, end_date])
                context['whosale_amount_dates'] = whosale_amount_dates
                context['retail_amount_dates'] = retail_amount_dates
                context['sales_count_dates'] = sales_count_dates

            #### Graphic Data
            sale = Sale.objects.filter(products__product__in=[product])
            qss = qsstats.QuerySetStats(sale, 'created', aggregate=Sum('products__count'))

            seven_days_ago = timezone.localtime(timezone.now() - datetime.timedelta(days=7))
            time_series = qss.time_series(seven_days_ago, timezone.localtime(timezone.now()))
            context['graphic'] = [t[1] for t in time_series]
            context['graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

            one_month_ago = timezone.localtime(timezone.now() - relativedelta(months=1))
            time_series = qss.time_series(one_month_ago, timezone.localtime(timezone.now()), interval='days')
            context['month_graphic'] = [t[1] for t in time_series]
            context['month_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

            one_year_ago = timezone.localtime(timezone.now() - relativedelta(years=1))
            time_series = qss.time_series(one_year_ago, timezone.localtime(timezone.now()), interval='months')
            context['year_graphic'] = [t[1] for t in time_series]
            context['year_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

            start_date = self.request.GET.get('graphic_start_date', None)
            end_date = self.request.GET.get('graphic_end_date', None)
            context['graphic_start_date'] = start_date
            context['graphic_end_date'] = end_date
            if start_date and end_date:
                start_date = start_date.split('-')
                start_date = [int(x) for x in start_date]
                start_date = datetime.date(year=start_date[0], month=start_date[1], day=start_date[2])
                end_date = end_date.split('-')
                end_date = [int(x) for x in end_date]
                end_date = datetime.date(year=end_date[0], month=end_date[1], day=end_date[2])
                interval = end_date - start_date
                if interval > datetime.timedelta(days=91):
                    time_series = qss.time_series(start_date, end_date, interval='months')
                elif interval > datetime.timedelta(days=21):
                    time_series = qss.time_series(start_date, end_date, interval='weeks')
                else:
                    time_series = qss.time_series(start_date, end_date, interval='days')
                context['date_graphic'] = [t[1] for t in time_series]
                context['date_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]


        else:
            context['products'] = Product.objects.all()
            context['graphic'] = []
            context['graphic_labels'] = []
            context['month_graphic'] = []
            context['month_graphic_labels'] = []
            context['year_graphic'] = []
            context['year_graphic_labels'] = []
            context['graphic_start_date'] = None
            context['graphic_end_date'] = None
        return context
