from django.shortcuts import render


def handler404(request, *args, **argv):
    response = render('404.html', {}, )
    return response

def handler503(request, *args, **argv):
    response = render('503.html', {}, )
    return response

def handler500(request, *args, **argv):
    response = render('503.html', {}, )
    return response
