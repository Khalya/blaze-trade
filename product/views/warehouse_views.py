from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView

from product.forms import WarehouseModelForm
from product.models import Warehouse


class WarehouseListView(ListView):
    model = Warehouse
    fields = '__all__'
    template_name = 'warehouse/warehouse.html'
    context_object_name = 'warehouse'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(WarehouseListView, self).get_context_data(object_list=None, **kwargs)
        context['form'] = WarehouseModelForm()
        return context

    def post(self, request):
        if 'delete' in request.POST:
            instance = Warehouse.objects.filter(id=request.POST.get('id')).first()
            if instance:
                instance.delete()
        elif 'id' in request.POST:
            instance = Warehouse.objects.get(id=request.POST.get('id'))
            form = WarehouseModelForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
        else:
            form = WarehouseModelForm(request.POST)
            if form.is_valid():
                form.save()
        qs = self.get_queryset()
        return redirect(reverse_lazy('warehouse'))


class WarehouseDetailView(DetailView):
    model = Warehouse
    fields = '__all__'
    template_name = 'warehouse/products.html'
    context_object_name = 'warehouse'
