from django import template
from product.models import Warehouse, Cash

register = template.Library()


@register.simple_tag()
def multiply(count, value, *args, **kwargs):
    # you would need to do any localization of the result here
    if count and value:
        return float(count) * float(value)
    else:
        return 0


@register.simple_tag()
def plus(count, value, *args, **kwargs):
    # you would need to do any localization of the result here
    return float(count) + float(value)


@register.simple_tag()
def minus(count, value, *args, **kwargs):
    # you would need to do any localization of the result here
    return int(count) - int(value)


@register.simple_tag()
def correct_input(value, *args, **kwargs):
    return str(value).replace(',', '.')


@register.simple_tag()
def cash_filter(value, *args, **kwargs):
    return Cash.objects.exclude(id=value)


@register.simple_tag()
def warehouse_filter(value, *args, **kwargs):
    return len(set([x.product for x in Warehouse.objects.get(id=value).delivery_set.all()]))

@register.simple_tag()
def warehouse_product_filter(value, *args, **kwargs):
    return set([x.product for x in Warehouse.objects.get(id=value).delivery_set.all()])
