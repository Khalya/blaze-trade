import datetime
import qsstats
from dateutil.relativedelta import relativedelta
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.utils import timezone
from django.views.generic import TemplateView, ListView

from core.forms import CompanyModelForm, ThemeModelForm, UserModelForm, UserUpdateForm, ExpenseModelForm, \
    ProductExpenseModelForm, CashModelForm
from core.models import Company, Balance, ProductExpense, Expense
from core.utils.helpers import get_amount, get_statistics_data, get_realtime_cash_data, get_statistics_data_user, \
    get_statistics_data_cash, get_statistics_data_casher_cash
from product.forms import ProductCountMessageModelForm
from product.models import Product, Sale, ProductCount, ProductCountMessage, Theme, Warehouse, StartSum, Cash
from user.models import User


class HomeView(TemplateView):
    template_name = 'index-3.html'

    def get(self, *args, **kwargs):
        if self.request.user.user_type == 'user':
            return redirect(reverse('cash'))
        # for key in list(self.request.session.keys()):
        #     del self.request.session[key]
        return super().get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['products'] = Product.objects.order_by('-purchased')[:10]
        context['report'] = self.request.GET.get('report')
        sale = Sale.objects.all()
        qss = qsstats.QuerySetStats(sale, 'created')

        seven_days_ago = timezone.localtime(timezone.now() - datetime.timedelta(days=7))
        time_series = qss.time_series(seven_days_ago, timezone.localtime(timezone.now()))
        context['graphic'] = [t[1] for t in time_series]
        context['graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

        one_month_ago = timezone.localtime(timezone.now() - relativedelta(months=1))
        time_series = qss.time_series(one_month_ago, timezone.localtime(timezone.now()), interval='days')
        context['month_graphic'] = [t[1] for t in time_series]
        context['month_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

        one_year_ago = timezone.localtime(timezone.now() - relativedelta(years=1))
        time_series = qss.time_series(one_year_ago, timezone.localtime(timezone.now()), interval='months')
        context['year_graphic'] = [t[1] for t in time_series]
        context['year_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

        start_date = self.request.GET.get('graphic_start_date', None)
        end_date = self.request.GET.get('graphic_end_date', None)
        context['graphic_start_date'] = start_date
        context['graphic_end_date'] = end_date
        if start_date and end_date:
            start_date = start_date.split('-')
            start_date = [int(x) for x in start_date]
            start_date = datetime.date(year=start_date[0], month=start_date[1], day=start_date[2])
            end_date = end_date.split('-')
            end_date = [int(x) for x in end_date]
            end_date = datetime.date(year=end_date[0], month=end_date[1], day=end_date[2])
            interval = end_date - start_date
            if interval > datetime.timedelta(days=91):
                time_series = qss.time_series(start_date, end_date, interval='months')
            elif interval > datetime.timedelta(days=21):
                time_series = qss.time_series(start_date, end_date, interval='weeks')
            else:
                time_series = qss.time_series(start_date, end_date, interval='days')
            context['date_graphic'] = [t[1] for t in time_series]
            context['date_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

        week_whosale_amount, week_get_always_full_amount, week_sales_count, week_middle_receipt = get_statistics_data(
            interval='weeks')
        context['week_get_always_full_amount'] = week_get_always_full_amount
        context['week_whosale_amount'] = week_whosale_amount
        context['week_middle_receipt'] = week_middle_receipt
        context['week_sales_count'] = week_sales_count

        month_whosale_amount, month_get_always_full_amount, month_sales_count, month_middle_receipt = get_statistics_data(
            interval='month')
        context['month_get_always_full_amount'] = month_get_always_full_amount
        context['month_whosale_amount'] = month_whosale_amount
        context['month_middle_receipt'] = month_middle_receipt
        context['month_sales_count'] = month_sales_count

        year_whosale_amount, year_get_always_full_amount, year_sales_count, year_middle_receipt = get_statistics_data(
            interval='years')
        context['year_get_always_full_amount'] = year_get_always_full_amount
        context['year_whosale_amount'] = year_whosale_amount
        context['year_middle_receipt'] = year_middle_receipt
        context['year_sales_count'] = year_sales_count

        whosale_amount, get_always_full_amount, retail_amount_card, sales_count, middle_receipt = get_realtime_cash_data(
            interval='days')

        context['week_get_always_full_amount'] = week_get_always_full_amount
        context['week_whosale_amount'] = whosale_amount
        context['week_middle_receipt'] = week_middle_receipt
        context['week_sales_count'] = week_sales_count

        yesterday_whosale_amount, yesterday_get_always_full_amount, retail_amount_card, yesterday_sales_count, yesterday_middle_receipt = get_realtime_cash_data(
            interval='yesterday')

        context['yesterday_get_always_full_amount'] = yesterday_get_always_full_amount
        context['yesterday_whosale_amount'] = yesterday_whosale_amount
        context['yesterday_middle_receipt'] = yesterday_middle_receipt
        context['yesterday_sales_count'] = yesterday_sales_count
        context['yesterday_retail_amount_card'] = retail_amount_card

        whosale_amount, get_always_full_amount, retail_amount_card, sales_count, middle_receipt = get_realtime_cash_data(
            interval='days')

        context['get_always_full_amount'] = get_always_full_amount
        context['whosale_amount'] = whosale_amount
        context['middle_receipt'] = middle_receipt
        context['sales_count'] = sales_count
        context['retail_amount_card'] = retail_amount_card

        start_date = self.request.GET.get('start_date', None)
        end_date = self.request.GET.get('end_date', None)
        context['start_date'] = start_date
        context['end_date'] = end_date
        if start_date and end_date:
            start_date = start_date.split('-')
            start_date = [int(x) for x in start_date]
            start_date = datetime.date(year=start_date[0], month=start_date[1], day=start_date[2])
            end_date = end_date.split('-')
            end_date = [int(x) for x in end_date]
            end_date = datetime.date(year=end_date[0], month=end_date[1], day=end_date[2])
            interval = end_date - start_date
            if interval <= datetime.timedelta(days=7):
                period_whosale_amount, period_retail_amount_classic, period_retail_amount_card, period_sales_count, period_middle_receipt = get_realtime_cash_data(
                    date=[start_date, end_date]
                )
                context['period_get_always_full_amount'] = period_retail_amount_classic
                context['period_retail_amount_card'] = period_retail_amount_card

            else:
                period_whosale_amount, period_get_always_full_amount, period_sales_count, period_middle_receipt = get_statistics_data(
                    date=[start_date, end_date])
                context['period_get_always_full_amount'] = period_get_always_full_amount
            context['period_whosale_amount'] = period_whosale_amount
            context['period_middle_receipt'] = period_middle_receipt
            context['period_sales_count'] = period_sales_count
        # create_messages()
        context['warehouse'] = Warehouse.objects.first()
        return context


class CashView(TemplateView):
    template_name = 'cash.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # for key in list(self.request.session.keys()):
        #     del self.request.session[key]
        if self.request.user.cash.all():
            obj, _ = StartSum.objects.get_or_create(
                cash_id=self.request.user.cash.all().first().id,
                defaults={
                    'cash_id': self.request.user.cash.all().first().id,
                    'amount': 0
                }
            )
            context['start_sum'] = obj

            whosale_amount, get_always_full_amount, retail_amount_card, sales_count, middle_receipt = get_statistics_data_casher_cash(
                cash=self.request.user.cash.all().first().id,
                interval='days')

            context['get_always_full_amount'] = get_always_full_amount + obj.amount
            context['whosale_amount'] = whosale_amount
            context['middle_receipt'] = middle_receipt
            context['sales_count'] = sales_count
            context['retail_amount_card'] = retail_amount_card
        else:
            context['cashes'] = Cash.objects.filter(Q(user__is_start=False) | Q(user__isnull=True))

        return context


def set_cash(request):
    today = timezone.now()
    cash = request.POST.get('cash')
    if cash:
        user = request.user
        user.start_work = today
        user.is_start = True
        user.end_work = None
        user.is_end = False
        user.save()
        obj = Cash.objects.get(id=cash)
        obj.user = user
        obj.save()
        return JsonResponse({
            'msg': 'ok'
        })
    else:
        return JsonResponse({
            'msg': 'ok'
        })


def update_start_sum(request):
    amount = request.POST.get('amount', 0)
    cash_id = request.user.cash.all().first().id
    start_sum = StartSum.objects.get(cash_id=cash_id)
    start_sum.amount = amount
    start_sum.save()
    return JsonResponse({
        'amount': amount
    })


# 2001111002345
def search_product(request):
    article = request.POST.get('article')
    in_kg = request.POST.get('kg', False)
    try:
        if in_kg:
            product_code = int(article[3:7])
            kg = article[7:13]
            article = product_code
            kg = int(kg)
            count = kg / 10000
        product = Product.objects.get(article=article)
        if in_kg and product.article not in request.session['articles_buy']:
            data = {
                article: {
                    'id': product.id,
                    'count': count,
                    'title': product.title,
                    'price': product.retail_price,
                    'amount': round(product.retail_price * count, 2),
                    'kg': True
                }
            }
            request.session['articles_buy'].update(data)
            request.session.modified = True
            return JsonResponse({
                'article': product.article,
                'title': product.title,
                'price': product.retail_price,
                'count': request.session['articles_buy'][int(product.article)]['count'],
                'full_amount': get_amount(request),
                'kg': in_kg
            })
        elif product.unit == 'unit':
            if product.article in request.session['articles_buy']:
                request.session['articles_buy'][product.article]['count'] += 1
                request.session['articles_buy'][product.article]['amount'] = \
                    request.session['articles_buy'][product.article]['price'] * \
                    request.session['articles_buy'][product.article]['count']
            else:
                request.session['articles_buy'].update({
                    article: {
                        'id': product.id,
                        'count': 1,
                        'title': product.title,
                        'price': product.retail_price,
                        'amount': 1 * product.retail_price
                    }
                })
            request.session.modified = True
            return JsonResponse({
                'article': product.article,
                'title': product.title,
                'price': product.retail_price,
                'count': request.session['articles_buy'][product.article]['count'],
                'full_amount': get_amount(request),
                'kg': in_kg
            })
        return JsonResponse({
            'article': product.article,
            'title': product.title,
            'price': product.retail_price,
        })
    except Product.DoesNotExist:
        return JsonResponse({
            'msg': 'Продукт не найден'
        }, status=400)


def cash(request):
    article = request.POST.get('article')
    product_count = request.POST.get('count')
    product_count = int(product_count)
    request.session.setdefault('articles', [])
    request.session.setdefault('articles_buy', {})
    request.session.setdefault('full_amount', 0)
    request.session.setdefault('whosale_amount', 0)
    request.session.setdefault('sales_count', 0)
    request.session.get('articles_buy', {}).setdefault('not_articles', {})
    in_kg = request.POST.get('kg', False)
    product_code = None
    kg = None
    if in_kg:
        product_code = int(article[3:7])
        kg = article[7:13]
        article = product_code
        kg = int(kg)

    try:
        product = Product.objects.get(article=article)
        if product.unit == 'kg':
            product_count /= 1000
        request.session['articles'] += [article]
        # count = request.session['articles'].count(article)
        if str(article) in request.session['articles_buy']:
            if not in_kg:
                request.session['articles_buy'][article]['count'] = product_count
                count = request.session['articles_buy'][article]['count']
                price = request.session['articles_buy'][article]['price']
                request.session['articles_buy'][article]['amount'] = product_count * price
        else:
            if in_kg or product.unit == 'kg':
                if product.unit == 'kg':
                    count = product_count
                else:
                    count = kg / 10000
                data = {
                    article: {
                        'id': product.id,
                        'count': count,
                        'title': product.title,
                        'price': product.retail_price,
                        'amount': round(product.retail_price * count, 2),
                        'kg': True
                    }
                }
                request.session['articles_buy'].update(data)
            else:
                request.session['articles_buy'].update({
                    article: {
                        'id': product.id,
                        'count': product_count,
                        'title': product.title,
                        'price': product.retail_price,
                        'whosale_price': product.wholesale_price,
                        'whosale_amount': product_count * product.wholesale_price,
                        'amount': round(product_count * product.retail_price, 2)
                    }
                })
        amount = get_amount(request)
        whosale_amount, full_amount, sales_count, middle_receipt = get_statistics_data(interval='days')
        request.session['full_amount'] = amount
        request.session['whosale_amount'] = full_amount - whosale_amount
        request.session['sales_count'] = sales_count
        request.session['get_always_full_amount'] = full_amount
        request.session.modified = True
    except Product.DoesNotExist:
        return JsonResponse({
            'msg': 'Продукт не найден'
        }, status=400)
    return JsonResponse({
        'article': product.article,
        'title': product.title,
        'price': product.retail_price,
        'count': product_count if not in_kg else kg / 10000,
        'full_amount': amount,
        'whosale_amount': whosale_amount,
        'sales_count': sales_count,
        'get_always_full_amount': full_amount,
        'kg': in_kg,
        'unit': product.unit
    })


def start_work(request):
    today = timezone.now()
    user = request.user
    user.start_work = today
    user.is_start = True
    user.end_work = None
    user.is_end = False
    user.save()
    return JsonResponse({
        'msg': "Ok"
    })


def end_work(request):
    today = timezone.now()
    user = request.user
    user.end_work = today
    user.is_end = True
    user.is_start = False
    user.save()
    return JsonResponse({
        'msg': "Ok"
    })


def add_product(request):
    request.session.setdefault('articles_buy', {})
    request.session.get('articles_buy', {}).setdefault('not_articles', {})
    title = request.POST.get('title')
    price = request.POST.get('price')
    count = request.POST.get('count')
    product, _ = Product.objects.get_or_create(
        title=title,
        defaults={
            'title': title,
            'price': price,
        }
    )
    request.session.get('articles_buy')['not_articles'].update({
        title: {
            'price': price,
            'count': count,
            'amount': int(price) * int(count),
            'id': product.id
        }
    })
    request.session['full_amount'] = int(get_amount(request))
    request.session.modified = True
    return JsonResponse({
        'title': title,
        'price': int(price),
        'count': int(count),
        'amount': int(price) * int(count)
    })


def cash_buy(request):
    request.session.setdefault('articles', [])
    request.session.setdefault('articles_buy', {})
    request.session.get('articles_buy', {}).setdefault('not_articles', {})
    request.session.setdefault('full_amount', 0)
    articles_data = request.session['articles_buy']
    not_articles_data = request.session.get('articles_buy')['not_articles']
    check_articles = articles_data.copy()
    if 'not_articles' in check_articles:
        del check_articles['not_articles']
    if not check_articles and not not_articles_data:
        return JsonResponse({
            'error': 'Пожалуйста добавьте товары'
        })
    amount = request.POST.get('amount')
    payment_type = request.POST.get('payment_type')
    request.session['full_amount'] = get_amount(request)
    full_amount = request.session['full_amount']
    request.session.modified = True
    sell_amount = float(amount) - request.session['full_amount']
    check_data = []
    sale = Sale.objects.create(
        payment_type=payment_type,
        amount=full_amount,
        cash_id=request.user.cash.all().first().id
    )
    balance = Balance.objects.first()
    balance.amount += full_amount
    balance.save()
    for key, value in articles_data.items():
        if value.get('id'):
            product = ProductCount(
                product_id=value['id'],
                count=value['count']
            )
            product.save()
            sale.products.add(product)

            check_data.append({
                'title': value['title'],
                'price': value['price'],
                'count': value['count'],
                'amount': value['count'] * value['price']
            })
    request.session['articles_buy'] = {}
    request.session['articles'] = []
    request.session['full_amount'] = 0
    request.session['not_articles_buy'] = {}
    request.session.modified = True
    context = {
        'company_name': Company.objects.first().title,
        'full_amount': full_amount,
        'products': check_data,
        'amount': int(sell_amount),
        'today': datetime.datetime.now().strftime('%Y-%m-%d'),
        'username': request.user.first_name,
        'cash_id': request.user.cash.all().first().id
    }
    if sell_amount >= 0:
        return JsonResponse({
            'msg': f"Сдача: {sell_amount} сум",
            'data': context,
            'sell_amount': sell_amount
        })
    else:
        return JsonResponse({
            'msg': f"Недосдача {sell_amount * -1} сум",
            'data': context,
            'sell_amount': sell_amount
        })


def remove_product(request):
    key = request.POST.get('key', None)
    del request.session['articles_buy'][str(key)]
    request.session.modified = True
    try:
        index = request.session['articles'].index(str(key))
        del request.session['articles'][index]
    except ValueError:
        pass
    request.session['full_amount'] = get_amount(request)
    request.session.modified = True
    return JsonResponse({
        'msg': 'ok'
    })


class MyCompany(TemplateView):
    template_name = 'company/my_company.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        company = Company.objects.first()
        context['company'] = company
        context['form'] = CompanyModelForm(instance=company)
        product_count = ProductCountMessage.objects.first()
        theme = Theme.objects.filter(user=self.request.user).first()
        if theme:
            context['theme_form'] = ThemeModelForm(instance=theme)
        else:
            context['theme_form'] = ThemeModelForm()
        if product_count:
            context['product_form'] = ProductCountMessageModelForm(instance=product_count)
        else:
            context['product_form'] = ProductCountMessageModelForm()
        return context

    def post(self, request):
        company = Company.objects.first()
        product_count = ProductCountMessage.objects.first()
        theme = Theme.objects.filter(user=self.request.user).first()
        form = CompanyModelForm(request.POST, instance=company)
        if theme:
            theme_form = ThemeModelForm(request.POST, instance=theme)
        else:
            theme_form = ThemeModelForm(request.POST)
        if product_count:
            product_form = ProductCountMessageModelForm(request.POST, instance=product_count)
        else:
            product_form = ProductCountMessageModelForm(request.POST)
        if form.is_valid():
            form.save()
        elif theme_form.is_valid() or product_form.is_valid():
            form = theme_form.save(commit=False)
            form.user = request.user
            form.save()
            if product_form.is_valid():
                product_form.save()
        return redirect(reverse_lazy('my_company'))


class DutyTemplateView(TemplateView):
    template_name = 'duty/duty.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['directors'] = User.objects.prefetch_related('cash').filter(user_type='admin')
        context['сashiers'] = User.objects.prefetch_related('cash').filter(user_type='user')
        context['managers'] = User.objects.prefetch_related('cash').filter(user_type='product_manager')
        context['form'] = UserModelForm()
        context['cash_form'] = CashModelForm()
        return context

    def post(self, request):
        response = redirect(reverse_lazy('duty'))
        if 'add_cash' in request.POST:
            form = CashModelForm(request.POST)
            if form.is_valid():
                form.save()
            else:
                print(form.errors)
            return response
        elif 'delete_cash' in request.POST:
            id = request.POST.get('id', None)
            if id:
                Cash.objects.get(id=id).delete()
                return response
        elif 'update' in request.POST:
            user = request.POST.get('id')
            instance = User.objects.get(id=user)
            form = UserUpdateForm(request.POST, instance=instance)
            if form.is_valid():
                instance = form.save()
                cash = request.POST.get('cash')
                if cash:
                    cash = Cash.objects.get(id=cash)
                    cash.user = instance
                    cash.save()
                return response
            else:
                print(form.errors)
        elif 'delete' in request.POST:
            user = request.POST.get('id')
            User.objects.get(id=user).delete()
            return response
        else:
            form = UserModelForm(request.POST)
            cash = request.POST.get('cash')
            if form.is_valid():
                instance = form.save()
                if cash:
                    cash = Cash.objects.get(id=cash)
                    cash.user = instance
                    cash.save()
                return response
            else:
                print(form.errors)


class ExpenseTemplateView(TemplateView):
    template_name = 'expense/expense.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['expenses'] = Expense.objects.order_by('-created')
        context['form'] = ExpenseModelForm()
        return context

    def post(self, request):
        response = redirect(reverse_lazy('expenses'))
        if 'update' in request.POST:
            id = request.POST.get('id')
            expense = Expense.objects.get(id=id)
            form = ExpenseModelForm(request.POST, instance=expense)
            if form.is_valid():
                form.save()
                return response
        elif 'delete' in request.POST:
            id = request.POST.get('id')
            expense = Expense.objects.get(id=id)
            expense.delete()
            return response
        else:
            form = ExpenseModelForm(request.POST)
            if form.is_valid():
                form = form.save(commit=False)
                form.user = request.user
                form.save()
                return response


class ProductExpenseTemplateView(TemplateView):
    template_name = 'expense/product_expense.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['expenses'] = ProductExpense.objects.order_by('-created')
        context['form'] = ProductExpenseModelForm()
        return context

    def post(self, request):
        response = redirect(reverse_lazy('expenses_product'))
        if 'update' in request.POST:
            id = request.POST.get('id')
            expense = ProductExpense.objects.get(id=id)
            form = ProductExpenseModelForm(request.POST, instance=expense)
            if form.is_valid():
                form.save()
                return response
        elif 'delete' in request.POST:
            id = request.POST.get('id')
            expense = ProductExpense.objects.get(id=id)
            expense.delete()
            return response
        else:
            form = ProductExpenseModelForm(request.POST)
            if form.is_valid():
                form = form.save(commit=False)
                form.user = request.user
                form.save()
                return response


class UsersAnalyticView(TemplateView):
    template_name = 'analytic/analytic_users.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user_get = self.request.GET.get('user', None)
        if user_get:

            #### Simple Data
            user = User.objects.get(id=user_get)
            context['user'] = user
            context['user_id'] = user_get
            context['users'] = User.objects.exclude(id=user_get)

            whosale_amount, retail_amount, sales_count = get_statistics_data_user(user)
            context['whosale_amount_day'] = whosale_amount
            context['retail_amount_day'] = retail_amount
            context['sales_count_day'] = sales_count

            whosale_amount_yesterday, retail_amount_yesterday, sales_count_yesterday \
                = get_statistics_data_user(user, interval='yesterday')
            context['whosale_amount_yesterday'] = whosale_amount_yesterday
            context['retail_amount_yesterday'] = retail_amount_yesterday
            context['sales_count_yesterday'] = sales_count_yesterday

            whosale_amount_weeks, retail_amount_weeks, sales_count_weeks \
                = get_statistics_data_user(user, interval='weeks')
            context['whosale_amount_weeks'] = whosale_amount_weeks
            context['retail_amount_weeks'] = retail_amount_weeks
            context['sales_count_weeks'] = sales_count_weeks

            whosale_amount_month, retail_amount_month, sales_count_month \
                = get_statistics_data_user(user, interval='month')
            context['whosale_amount_month'] = whosale_amount_month
            context['retail_amount_month'] = retail_amount_month
            context['sales_count_month'] = sales_count_month

            whosale_amount_years, retail_amount_years, sales_count_years \
                = get_statistics_data_user(user, interval='years')
            context['whosale_amount_years'] = whosale_amount_years
            context['retail_amount_years'] = retail_amount_years
            context['sales_count_years'] = sales_count_years

            start_date = self.request.GET.get('start_date', None)
            end_date = self.request.GET.get('end_date', None)
            context['start_date'] = start_date
            context['end_date'] = end_date
            if start_date and end_date:
                whosale_amount_dates, retail_amount_dates, sales_count_dates \
                    = get_statistics_data_user(user, date=[start_date, end_date])
                context['whosale_amount_dates'] = whosale_amount_dates
                context['retail_amount_dates'] = retail_amount_dates
                context['sales_count_dates'] = sales_count_dates

            #### Graphic Data
            sale = Sale.objects.filter(cash__user_id=user.id)

            qss = qsstats.QuerySetStats(sale, 'created')

            seven_days_ago = timezone.localtime(timezone.now() - datetime.timedelta(days=7))
            time_series = qss.time_series(seven_days_ago, timezone.localtime(timezone.now()))
            context['graphic'] = [t[1] for t in time_series]
            context['graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]
            print(context['graphic'])

            one_month_ago = timezone.localtime(timezone.now() - relativedelta(months=1))
            time_series = qss.time_series(one_month_ago, timezone.localtime(timezone.now()), interval='days')
            context['month_graphic'] = [t[1] for t in time_series]
            context['month_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

            one_year_ago = timezone.localtime(timezone.now() - relativedelta(years=1))
            time_series = qss.time_series(one_year_ago, timezone.localtime(timezone.now()), interval='months')
            context['year_graphic'] = [t[1] for t in time_series]
            context['year_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

            start_date = self.request.GET.get('graphic_start_date', None)
            end_date = self.request.GET.get('graphic_end_date', None)
            context['graphic_start_date'] = start_date
            context['graphic_end_date'] = end_date
            if start_date and end_date:
                start_date = start_date.split('-')
                start_date = [int(x) for x in start_date]
                start_date = datetime.date(year=start_date[0], month=start_date[1], day=start_date[2])
                end_date = end_date.split('-')
                end_date = [int(x) for x in end_date]
                end_date = datetime.date(year=end_date[0], month=end_date[1], day=end_date[2])
                interval = end_date - start_date
                if interval > datetime.timedelta(days=91):
                    time_series = qss.time_series(start_date, end_date, interval='months')
                elif interval > datetime.timedelta(days=21):
                    time_series = qss.time_series(start_date, end_date, interval='weeks')
                else:
                    time_series = qss.time_series(start_date, end_date, interval='days')
                context['date_graphic'] = [t[1] for t in time_series]
                context['date_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]


        else:
            context['users'] = User.objects.all()
            context['graphic'] = []
            context['graphic_labels'] = []
            context['month_graphic'] = []
            context['month_graphic_labels'] = []
            context['year_graphic'] = []
            context['year_graphic_labels'] = []
            context['graphic_start_date'] = None
            context['graphic_end_date'] = None
        return context


class CashAnalyticView(TemplateView):
    template_name = 'analytic/analytic_cash.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cash_get = self.request.GET.get('cash', None)
        if cash_get:

            #### Simple Data
            cash = Cash.objects.get(id=cash_get)
            context['cash'] = cash
            context['cash_id'] = cash_get
            context['cashes'] = Cash.objects.exclude(id=cash_get)

            whosale_amount, retail_amount, sales_count = get_statistics_data_cash(cash.id)
            context['whosale_amount_day'] = whosale_amount
            context['retail_amount_day'] = retail_amount
            context['sales_count_day'] = sales_count

            whosale_amount_yesterday, retail_amount_yesterday, sales_count_yesterday \
                = get_statistics_data_cash(cash.id, interval='yesterday')
            context['whosale_amount_yesterday'] = whosale_amount_yesterday
            context['retail_amount_yesterday'] = retail_amount_yesterday
            context['sales_count_yesterday'] = sales_count_yesterday

            whosale_amount_weeks, retail_amount_weeks, sales_count_weeks \
                = get_statistics_data_cash(cash.id, interval='weeks')
            context['whosale_amount_weeks'] = whosale_amount_weeks
            context['retail_amount_weeks'] = retail_amount_weeks
            context['sales_count_weeks'] = sales_count_weeks

            whosale_amount_month, retail_amount_month, sales_count_month \
                = get_statistics_data_cash(cash.id, interval='month')
            context['whosale_amount_month'] = whosale_amount_month
            context['retail_amount_month'] = retail_amount_month
            context['sales_count_month'] = sales_count_month

            whosale_amount_years, retail_amount_years, sales_count_years \
                = get_statistics_data_cash(cash.id, interval='years')
            context['whosale_amount_years'] = whosale_amount_years
            context['retail_amount_years'] = retail_amount_years
            context['sales_count_years'] = sales_count_years

            start_date = self.request.GET.get('start_date', None)
            end_date = self.request.GET.get('end_date', None)
            context['start_date'] = start_date
            context['end_date'] = end_date
            if start_date and end_date:
                whosale_amount_dates, retail_amount_dates, sales_count_dates \
                    = get_statistics_data_cash(cash.id, date=[start_date, end_date])
                context['whosale_amount_dates'] = whosale_amount_dates
                context['retail_amount_dates'] = retail_amount_dates
                context['sales_count_dates'] = sales_count_dates

            #### Graphic Data
            sale = Sale.objects.filter(cash_id=cash.id)

            qss = qsstats.QuerySetStats(sale, 'created')

            seven_days_ago = timezone.localtime(timezone.now() - datetime.timedelta(days=7))
            time_series = qss.time_series(seven_days_ago, timezone.localtime(timezone.now()))
            context['graphic'] = [t[1] for t in time_series]
            context['graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

            one_month_ago = timezone.localtime(timezone.now() - relativedelta(months=1))
            time_series = qss.time_series(one_month_ago, timezone.localtime(timezone.now()), interval='days')
            context['month_graphic'] = [t[1] for t in time_series]
            context['month_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

            one_year_ago = timezone.localtime(timezone.now() - relativedelta(years=1))
            time_series = qss.time_series(one_year_ago, timezone.localtime(timezone.now()), interval='months')
            context['year_graphic'] = [t[1] for t in time_series]
            context['year_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]

            start_date = self.request.GET.get('graphic_start_date', None)
            end_date = self.request.GET.get('graphic_end_date', None)
            context['graphic_start_date'] = start_date
            context['graphic_end_date'] = end_date
            if start_date and end_date:
                start_date = start_date.split('-')
                start_date = [int(x) for x in start_date]
                start_date = datetime.date(year=start_date[0], month=start_date[1], day=start_date[2])
                end_date = end_date.split('-')
                end_date = [int(x) for x in end_date]
                end_date = datetime.date(year=end_date[0], month=end_date[1], day=end_date[2])
                interval = end_date - start_date
                if interval > datetime.timedelta(days=91):
                    time_series = qss.time_series(start_date, end_date, interval='months')
                elif interval > datetime.timedelta(days=21):
                    time_series = qss.time_series(start_date, end_date, interval='weeks')
                else:
                    time_series = qss.time_series(start_date, end_date, interval='days')
                context['date_graphic'] = [t[1] for t in time_series]
                context['date_graphic_labels'] = [t[0].strftime('%Y-%m-%d') for t in time_series]


        else:
            context['users'] = User.objects.all()
            context['graphic'] = []
            context['graphic_labels'] = []
            context['month_graphic'] = []
            context['month_graphic_labels'] = []
            context['year_graphic'] = []
            context['year_graphic_labels'] = []
            context['graphic_start_date'] = None
            context['graphic_end_date'] = None
        return context


class SalesListView(ListView):
    template_name = 'analytic/sales.html'
    model = Sale
    context_object_name = 'sales'
    queryset = Sale.objects.order_by('-created')

    def get_queryset(self):
        if self.request.GET.get('product'):
            return Sale.objects.filter(products__product_id__in=[self.request.GET.get('product')])
        else:
            return super().get_queryset()

    def get_context_data(self, **kwargs):
        context = super(SalesListView, self).get_context_data(**kwargs)
        context['products'] = Product.objects.order_by('-id')
        if self.request.GET.get('product'):
            context['product'] = Product.objects.get(id=self.request.GET.get('product'))
        return context

    def post(self, request):
        if 'delete' in request.POST:
            Sale.objects.get(id=request.POST.get('id')).delete()
            return redirect(reverse_lazy('sales'))
