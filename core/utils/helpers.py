import datetime
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from product.models import Sale


def get_amount(request):
    full_amount = 0
    # if request.session.get('articles', None):
    for article in list(request.session['articles_buy'].keys()):
        if request.session['articles_buy'][article].get('amount', None):
            full_amount += request.session['articles_buy'][article]['amount']
    # for key in list(request.session.get('articles_buy')['not_articles'].keys()):
    #     full_amount += request.session.get('articles_buy')['not_articles'][key]['amount']
    # print(full_amount)
    # print(request.session['articles_buy'], full_amount)
    return full_amount


# def get_whosale_price(interval='days', date=None):
#     if date:
#         sales = Sale.objects.filter(created__range=[date[0], date[1]])
#     elif interval == 'days':
#         sales = Sale.objects.filter(created=timezone.localtime(timezone.now()))
#     elif interval == 'yesterday':
#         sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)))
#     elif interval == 'weeks':
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - datetime.timedelta(days=7)),
#                                                     timezone.localtime(timezone.now())])
#     elif interval == 'month':
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(months=1)),
#                                                     timezone.localtime(timezone.now())])
#     else:
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(years=1)),
#                                                     timezone.localtime(timezone.now())])
#     total = 0
#     for sale in sales:
#         for product in sale.products.all():
#             total += product.product.wholesale_price
#     return total
#
#
#
# def get_full_amount(interval='days', date=None):
#     if date:
#         sales = Sale.objects.filter(created__range=[date[0], date[1]])
#     elif interval == 'days':
#         sales = Sale.objects.filter(created=timezone.localtime(timezone.now()))
#     elif interval == 'yesterday':
#         sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)))
#     elif interval == 'weeks':
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - datetime.timedelta(days=7)),
#                                                     timezone.localtime(timezone.now())])
#     elif interval == 'month':
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(months=1)),
#                                                     timezone.localtime(timezone.now())])
#     else:
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(years=1)),
#                                                     timezone.localtime(timezone.now())])
#     total = 0
#     for sale in sales:
#         for product in sale.products.all():
#             total += product.product.retail_price
#     return total
#
#
# def get_sales_count(interval='days', date=None):
#     if date:
#         sales = Sale.objects.filter(created__range=[date[0], date[1]]).count()
#     elif interval == 'days':
#         sales = Sale.objects.filter(created=timezone.localtime(timezone.now())).count()
#     elif interval == 'yesterday':
#         sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1))).count()
#     elif interval == 'weeks':
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - datetime.timedelta(days=7)),
#                                                     timezone.localtime(timezone.now())]).count()
#     elif interval == 'month':
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(months=1)),
#                                                     timezone.localtime(timezone.now())]).count()
#     else:
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(years=1)),
#                                                     timezone.localtime(timezone.now())]).count()
#     return sales
#
#
# def get_middle_receipt(interval='days', date=None):
#     if date:
#         sales = Sale.objects.filter(created__range=[date[0], date[1]])
#     elif interval == 'days':
#         sales = Sale.objects.filter(created=timezone.localtime(timezone.now()))
#     elif interval == 'yesterday':
#         sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)))
#     elif interval == 'weeks':
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - datetime.timedelta(days=7)),
#                                                     timezone.localtime(timezone.now())])
#     elif interval == 'month':
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(months=1)),
#                                                     timezone.localtime(timezone.now())])
#     else:
#         sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(years=1)),
#                                                     timezone.localtime(timezone.now())])
#     sale_count = sales.count()
#     amount = sales.aggregate(Sum('amount'))['amount__sum']
#     try:
#         return round((amount / sale_count), 2)
#     except TypeError:
#         return 0


def safety(request):
    request.session.setdefault('articles', [])
    request.session.setdefault('articles_buy', {})
    request.session.setdefault('full_amount', 0)
    request.session.setdefault('whosale_amount', 0)
    request.session.setdefault('sales_count', 0)
    request.session.setdefault('get_always_full_amount', 0)
    request.session.get('articles_buy', {}).setdefault('not_articles', {})


def get_statistics_data(interval='days', date=None):
    if date:
        sales = Sale.objects.filter(created__range=[date[0], date[1]])
    elif interval == 'days':
        sales = Sale.objects.filter(created=timezone.localtime(timezone.now()))
    elif interval == 'yesterday':
        sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)))
    elif interval == 'weeks':
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - datetime.timedelta(days=7)),
                                                    timezone.localtime(timezone.now())])
    elif interval == 'month':
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(months=1)),
                                                    timezone.localtime(timezone.now())])
    else:
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(years=1)),
                                                    timezone.localtime(timezone.now())])

    whosale_amount = 0
    retail_amount = 0
    for sale in sales:
        for product in sale.products.all():
            retail_amount += product.product.retail_price * product.count
            whosale_amount += product.product.difference * product.count
    sale_count = sales.count()
    try:
        middle_recipient = round((retail_amount / sale_count), 2)
        return whosale_amount, retail_amount, sale_count, middle_recipient
    except Exception:
        return whosale_amount, retail_amount, sale_count, 0


#
# def create_messages():
#     products = Product.objects.all()
#     product_count = ProductCountMessage.objects.first()
#     messages = []
#     for product in products:
#         if product_count:
#             if product.remainder <= product_count.count:
#                 message, created = Message.objects.get_or_create(product_id=product.id, defaults={
#                     'product_id': product.id,
#                     'title': "Нехватка продуктов !",
#                     "content": f"На складе осталось всего {product.remainder} {product.get_unit_display()} {product.title} \n нужно пополнить запасы !"
#                 })
#                 if created == False:
#                     message.delete()
#                 else:
#                     messages.append(message)
#
#         else:
#             if product.remainder <= 45:
#                 message, _ = Message.objects.get_or_create(product_id=product.id, defaults={
#                     'product_id': product.id,
#                     'title': "Нехватка продуктов !",
#                     "content": f"На складе осталось всего {product.remainder} {product.get_unit_display()} {product.title} \n нужно пополнить запасы !"
#                 })
#                 messages.append(message)
#     return messages


def get_statistics_data_product(product, interval='days', date=None):
    if date:
        sales = Sale.objects.filter(created__range=[date[0], date[1]], products__product__in=[product])
    elif interval == 'days':
        sales = Sale.objects.filter(created=timezone.localtime(timezone.now()), products__product__in=[product])
    elif interval == 'yesterday':
        sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)),
                                    products__product__in=[product])
    elif interval == 'weeks':
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - datetime.timedelta(days=7)),
                                                    timezone.localtime(timezone.now())],
                                    products__product__in=[product])
    elif interval == 'month':
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(months=1)),
                                                    timezone.localtime(timezone.now())],
                                    products__product__in=[product])
    else:
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(years=1)),
                                                    timezone.localtime(timezone.now())],
                                    products__product__in=[product])
    whosale_amount = 0
    retail_amount = 0
    sales_count = 0
    for sale in sales:
        for product_count in sale.products.all():
            if product_count.product_id == product.id:
                whosale_amount += product_count.product.difference * product_count.count
                retail_amount += product_count.product.retail_price * product_count.count
                sales_count += product_count.count
    return whosale_amount, retail_amount, sales_count


def get_statistics_data_user(user, interval='days', date=None) -> object:
    if date:
        sales = Sale.objects.filter(created__range=[date[0], date[1]], cash__user_id=user.id)
    elif interval == 'days':
        sales = Sale.objects.filter(created=timezone.localtime(timezone.now()), cash__user_id=user.id)
    elif interval == 'yesterday':
        sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)),
                                    cash__user_id=user.id)
    elif interval == 'weeks':
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - datetime.timedelta(days=7)),
                                                    timezone.localtime(timezone.now())], cash__user_id=user.id)
    elif interval == 'month':
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(months=1)),
                                                    timezone.localtime(timezone.now())], cash__user_id=user.id)
    else:
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(years=1)),
                                                    timezone.localtime(timezone.now())], cash__user_id=user.id)
    whosale_amount = 0
    retail_amount = 0
    for sale in sales:
        for product_count in sale.products.all():
            whosale_amount += product_count.product.difference * product_count.count
            retail_amount += product_count.product.retail_price * product_count.count
    sales_count = sales.count()
    return whosale_amount, retail_amount, sales_count


# whosale_amount, get_always_full_amount, retail_amount_card, sales_count, middle_receipt = get_statistics_data_cash(
#
#     interval='days')
def get_statistics_data_cash(cash, interval='days', date=None) -> object:
    if date:
        sales = Sale.objects.filter(created__range=[date[0], date[1]], cash_id=cash)
    elif interval == 'days':
        sales = Sale.objects.filter(created=timezone.localtime(timezone.now()), cash_id=cash)
    elif interval == 'yesterday':
        sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)),
                                    cash_id=cash)
    elif interval == 'weeks':
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - datetime.timedelta(days=7)),
                                                    timezone.localtime(timezone.now())], cash_id=cash)
    elif interval == 'month':
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(months=1)),
                                                    timezone.localtime(timezone.now())], cash_id=cash)
    else:
        sales = Sale.objects.filter(created__range=[timezone.localtime(timezone.now() - relativedelta(years=1)),
                                                    timezone.localtime(timezone.now())], cash_id=cash)
    whosale_amount = 0
    retail_amount = 0
    for sale in sales:
        for product_count in sale.products.all():
            whosale_amount += product_count.product.difference * product_count.count
            retail_amount += product_count.product.retail_price * product_count.count
    sales_count = sales.count()
    return whosale_amount, retail_amount, sales_count


def get_statistics_data_casher_cash(cash, interval='days', date=None) -> object:
    if date:
        simple_sales = Sale.objects.filter(created__range=[date[0], date[1]], payment_type='classic', cash_id=cash)
        card_sales = Sale.objects.filter(created__range=[date[0], date[1]], payment_type='card', cash_id=cash)
    elif interval == 'days':
        simple_sales = Sale.objects.filter(created=timezone.localtime(timezone.now()), payment_type='classic',
                                           cash_id=cash)
        card_sales = Sale.objects.filter(created=timezone.localtime(timezone.now()), payment_type='card', cash_id=cash)
    else:
        simple_sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)),
                                           payment_type='classic', cash_id=cash)
        card_sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)),
                                         payment_type='card', cash_id=cash)

    whosale_amount = 0
    retail_amount_classic = 0
    retail_amount = 0
    retail_amount_card = 0
    sales_count = 0

    for sale in simple_sales:
        for product in sale.products.all():
            retail_amount_classic += product.product.retail_price * product.count
            retail_amount += product.product.retail_price * product.count
            whosale_amount += product.product.difference * product.count
    sales_count += simple_sales.count()

    for sale in card_sales:
        for product in sale.products.all():
            retail_amount_card += product.product.retail_price * product.count
            retail_amount += product.product.retail_price * product.count
            whosale_amount += product.product.difference * product.count
    sales_count += card_sales.count()
    try:
        middle_receipt = round((retail_amount / sales_count), 2)
        return whosale_amount, retail_amount_classic, retail_amount_card, sales_count, middle_receipt
    except Exception:
        return whosale_amount, retail_amount_classic, retail_amount_card, sales_count, 0


def get_realtime_cash_data(interval='days', date=None):
    if date:
        simple_sales = Sale.objects.filter(created__range=[date[0], date[1]], payment_type='classic')
        card_sales = Sale.objects.filter(created__range=[date[0], date[1]], payment_type='card')
    elif interval == 'days':
        simple_sales = Sale.objects.filter(created=timezone.localtime(timezone.now()), payment_type='classic')
        card_sales = Sale.objects.filter(created=timezone.localtime(timezone.now()), payment_type='card')
    else:
        simple_sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)),
                                           payment_type='classic')
        card_sales = Sale.objects.filter(created=timezone.localtime(timezone.now() - datetime.timedelta(days=1)),
                                         payment_type='card')

    whosale_amount = 0
    retail_amount_classic = 0
    retail_amount = 0
    retail_amount_card = 0
    sales_count = 0

    for sale in simple_sales:
        for product in sale.products.all():
            retail_amount_classic += product.product.retail_price * product.count
            retail_amount += product.product.retail_price * product.count
            whosale_amount += product.product.difference * product.count
    sales_count += simple_sales.count()

    for sale in card_sales:
        for product in sale.products.all():
            retail_amount_card += product.product.retail_price * product.count
            retail_amount += product.product.retail_price * product.count
            whosale_amount += product.product.difference * product.count
    sales_count += card_sales.count()
    try:
        middle_receipt = round((retail_amount / sales_count), 2)
        return whosale_amount, retail_amount_classic, retail_amount_card, sales_count, middle_receipt
    except Exception:
        return whosale_amount, retail_amount_classic, retail_amount_card, sales_count, 0
