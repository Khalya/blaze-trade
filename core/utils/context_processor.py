from core.models import Company, Cash, Balance
from core.utils.helpers import safety, get_amount
from product.models import Message, Theme, TurnOver


def settings(request):
    if request.path != '/accounts/login/':
        safety(request)
        company = Company.objects.first()
        article_buy = request.session['articles_buy']

        not_articles_buy = article_buy.get('not_articles', {})
        messages = Message.objects.all()
        full_amount = get_amount(request)
        request.session.modified = True
        if 'not_articles' in article_buy:
            del article_buy['not_articles']
        balance, _ = Balance.objects.get_or_create(id=1, defaults={'amount': 0})
        turnover, _ = TurnOver.objects.get_or_create(id=1, defaults={'amount': 0})
        theme = Theme.objects.filter(user=request.user).first()
        if theme:
            theme = theme.theme
        else:
            theme = 'light'
        return {
            'company': company,
            'article_buy': article_buy,
            'full_amount': full_amount,
            'not_articles_buy': not_articles_buy,
            'article_buy_count': len(article_buy.values()),
            'messages': messages,
            'theme': theme,
            'cashes': Cash.objects.all(),
            'balance': balance,
            'turnover': turnover,
        }
    else:
        return {}
