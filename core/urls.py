from django.urls import path
from core.views import HomeView, CashView, cash, cash_buy, start_work, end_work, add_product, remove_product, MyCompany, \
    DutyTemplateView, ExpenseTemplateView, ProductExpenseTemplateView, UsersAnalyticView, SalesListView, \
    CashAnalyticView, update_start_sum, set_cash

urlpatterns = [
    path('', HomeView.as_view(), name='home'),

    path('cash/', CashView.as_view(), name='cash'),
    path('cash/realtime/', cash, name='realtime-cash'),
    path('cash/buy/', cash_buy, name='buy-cash'),
    path('cash/add_product/', add_product, name='add_product-cash'),
    path('cash/remove_product/', remove_product, name='remove_product-cash'),
    path('cash/start_work/', start_work, name='start_work-cash'),
    path('cash/end_work/', end_work, name='end_work-cash'),

    path('company/', MyCompany.as_view(), name='my_company'),
    path('duty/', DutyTemplateView.as_view(), name='duty'),

    path('expenses/', ExpenseTemplateView.as_view(), name='expenses'),
    path('expenses/product/', ProductExpenseTemplateView.as_view(), name='expenses_product'),

    path('user/analytic/', UsersAnalyticView.as_view(), name='user_analytic'),
    path('sales/analytic/', SalesListView.as_view(), name='sales'),
    path('cashes/analytic/', CashAnalyticView.as_view(), name='cashes'),

    path('update/start/sum/', update_start_sum, name='update_start_sum'),
    path('cash/set/', set_cash, name='set_cash'),
]
