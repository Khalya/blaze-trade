from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

from core.models import Company, Expense, ProductExpense
from product.models import Theme, Cash
from user.models import User


class CompanyModelForm(ModelForm):
    class Meta:
        model = Company
        fields = '__all__'


class ThemeModelForm(ModelForm):
    class Meta:
        model = Theme
        exclude = 'user',


class UserModelForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'phone_number', 'user_type',)


class UserUpdateForm(ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'phone_number', 'user_type',)


class ExpenseModelForm(ModelForm):
    class Meta:
        model = Expense
        exclude = 'user',


class ProductExpenseModelForm(ModelForm):
    class Meta:
        model = ProductExpense
        exclude = 'user',


class CashModelForm(ModelForm):
    class Meta:
        model = Cash
        fields = '__all__'
