from django.contrib import admin

from product.models import Cash
from .models import Company


@admin.register(Company)
@admin.register(Cash)
class AdminModel(admin.ModelAdmin):
    pass
