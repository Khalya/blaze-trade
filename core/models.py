from django.db import models
from user.models import User
from .utils import SingletonModel


class Company(SingletonModel):
    title = models.CharField(
        verbose_name='Название компании',
        max_length=255
    )
    inn = models.CharField(
        verbose_name='ИНН',
        max_length=255,
        default=''
    )
    payment_account = models.CharField(
        verbose_name='Расчетный счет',
        max_length=255,
        default=''
    )
    guvoxnoma_number = models.CharField(
        verbose_name='Номер гувохнома',
        max_length=255,
        default=''
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Настройки'
        verbose_name_plural = 'Настройки'


class Cash(models.Model):
    user = models.ForeignKey(
        User,
        verbose_name='Пользователь',
        on_delete=models.SET_NULL,
        null=True,
        related_name='cash',
        blank=True
    )

    def __str__(self):
        return str(self.user)

    class Meta:
        verbose_name = 'Касса'
        verbose_name_plural = 'Кассы'


class ProductExpense(models.Model):
    product = models.ForeignKey(
        to='product.Product',
        verbose_name='Продукт',
        on_delete=models.SET_NULL,
        null=True
    )
    count = models.FloatField(
        verbose_name='Количество',
        default=0
    )
    amount = models.FloatField(
        verbose_name='Сумма',
        default=0
    )
    user = models.ForeignKey(
        User,
        verbose_name='Пользователь',
        on_delete=models.SET_NULL,
        null=True
    )

    created = models.DateField(
        verbose_name='Дата',
        auto_now_add=True
    )

    def save(self, **kwargs):
        super().save(**kwargs)
        balance = Balance.objects.first()
        balance.amount -= self.amount
        balance.save()

    def delete(self, **kwargs):
        super().delete(**kwargs)

        balance = Balance.objects.first()
        balance.amount += self.amount
        balance.save()

    def __str__(self):
        return f"{self.user} | {self.amount}"

    class Meta:
        verbose_name = 'Расход на закуп'
        verbose_name_plural = 'Расходы на закуп'


class Expense(models.Model):
    title = models.CharField(
        verbose_name='Название',
        max_length=255
    )
    amount = models.FloatField(
        verbose_name='Сумма',
        default=0
    )
    user = models.ForeignKey(
        User,
        verbose_name='Пользователь',
        on_delete=models.SET_NULL,
        null=True
    )
    created = models.DateField(
        verbose_name='Дата',
        auto_now_add=True,
        null=True
    )
    def save(self, **kwargs):
        super().save(**kwargs)
        balance = Balance.objects.first()
        balance.amount -= self.amount
        balance.save()

    def delete(self, **kwargs):
        super().delete(**kwargs)

        balance = Balance.objects.first()
        balance.amount += self.amount
        balance.save()

    def __str__(self):
        return f"{self.title} | {self.amount}"

    class Meta:
        verbose_name = 'Доп. расходы'
        verbose_name_plural = 'Доп. расходы'


class Balance(models.Model):
    amount = models.FloatField(
        verbose_name='Сумма',
        default=0
    )

    def __str__(self):
        return str(self.amount)

    class Meta:
        verbose_name = 'Общий баланс'
        verbose_name_plural = 'Общий баланс'
